package GiaiPhuongTrinh;

import java.util.Scanner;

public class GiaiPTB1 {

	public static void main(String[] args) 
	{
		//ứng dụng giải phương trình bậc 1
		System.out.println("Chương trình GIẢI PHƯƠNG TRÌNH BẬC 1 DẠNG: Ax + B = 0");
		Scanner nhap = new Scanner(System.in);
		System.out.println("Mời bạn nhập hệ số a vào:");
		int a = nhap.nextInt();
		System.out.println("Mời bạn nhập hệ số B vào:");
		int b = nhap.nextInt();
		System.out.println("Chương trình GIẢI PHƯƠNG TRÌNH BẬC 1 DẠNG: "+a+"x + "+b+" = 0");
		GiaiPTB1(a, b);
		nhap.close();
	}

	private static void GiaiPTB1(int a, int b) 
	{
		if(a==0) // a bằng 0
		{
			if(b==0) // a bằng 0 và b bằng 0
			{
				System.out.println("Phương trình VÔ SỐ NGHIỆM");
			}
			else // a bằng 0 và b khác 0
			{
				System.out.println("Phương trình VÔ NGHIỆM");
			}
		}
		else// a khác 0
		{
			int kq = -b/a;
			System.out.println("Phương trình CÓ NGHIỆM X = "+kq);
		}

	}

}
