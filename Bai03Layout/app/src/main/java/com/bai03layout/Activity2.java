package com.bai03layout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by PC14-02 on 10/30/2015.
 */
public class Activity2 extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity2);
        //
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        Bundle bundle = intent.getExtras();
        int year = bundle.getInt("year");
        Hocsinh hs = bundle.getParcelable("hs");

        System.out.println(name);
        System.out.println(year);
        System.out.println(hs.getName());

    }
}
