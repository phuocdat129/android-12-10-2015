package com.bai03layout;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.LevelListDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.InputDevice;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.util.Scanner;

import static com.bai03layout.R.id.imageView;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener {

    int abc = 0;
    ImageView img;
    int state = 0;
    ImageView img4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //gan giao dien vao choa acitivy
        setContentView(R.layout.activity_main);

        Button btn = (Button) findViewById(R.id.button);
        btn.setOnClickListener(this);

        img = (ImageView) findViewById(R.id.imageView);
        SeekBar bar = (SeekBar) findViewById(R.id.seekBar);

        bar.setMax(70);
        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int value, boolean fromUser) {
                LevelListDrawable drawable = (LevelListDrawable) img.getBackground();
                drawable.setLevel(value);

                ClipDrawable clip = (ClipDrawable) img4.getDrawable();
                clip.setLevel(value * 100 );
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ImageView img2 = (ImageView) findViewById(R.id.imageView2);
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView imgV = (ImageView) v;
                TransitionDrawable drawable = (TransitionDrawable) imgV.getBackground();
                if(state == 0){
                    state = 1;
                    drawable.startTransition(500);
                }
                else{
                    state = 0;
                    drawable.reverseTransition(500);
                }
            }
        });

        img4 = (ImageView) findViewById(R.id.imageView4);

/*
        //Context
        Context context = this.getBaseContext();
        int color = context.getResources().getColor(R.color.xanh);

        Resources res = this.getResources();

        String msg = res.getString(R.string.dialog_dangnhap_sai);

        int tinnhan = 5;
        String  msg_dau = res.getString(R.string.thongbao_dau);
        String  msg_duoi = res.getString(R.string.thongbao_duoi);

        System.out.println(msg_dau + tinnhan + msg_duoi);

        String s1 = res.getString(R.string.thongbao_tinnhan, 1, 15);
        System.out.println(s1);

        String s2 = res.getString(R.string.thongbao_tinnhan);
        String s3 = String.format(s2, 2, 8);
        System.out.println(s3);

        String s4 = res.getString(R.string.button_title);
        System.out.println(s4);

        //Html.format
        //btn.setText(Html.fromHtml(s4));

        String s5 = res.getQuantityString(R.plurals.sach, 1);
        String s6 = res.getQuantityString(R.plurals.sach, 2);

        System.out.println(s5);
        System.out.println(s6);

        String[] s7 = res.getStringArray(R.array.gioitinh);

        TelephonyManager tel = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);
        String mnc = tel.getNetworkOperator();

        System.out.println("mnc: " + mnc);

        LayerDrawable draw1 = (LayerDrawable) this.getResources().getDrawable(R.drawable.layer_drawable);*/


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.button:

                Hocsinh hs = new Hocsinh();
                hs.setName("nguyen van teo");
                hs.setAddress("hcm");

                Bundle bundle = new Bundle();
                bundle.putInt("year", 2015);
                bundle.putParcelable("hs", hs);

                //this, Activity2.class
                Intent intent = new Intent();
                intent.setClass(this, Activity2.class);
                intent.putExtra("name", "nguyen van a");
                intent.putExtras(bundle);

                startActivity(intent);

                /*Uri uri = Uri.parse("");

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(uri);
                intent.setType("image*//*");

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);*/

                break;
        }

    }
}