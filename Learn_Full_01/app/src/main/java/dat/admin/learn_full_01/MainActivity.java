package dat.admin.learn_full_01;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {
    EditText editUser, editPass;
    Button btnLogin;
    CheckBox cbRemember;
    SharedPreferences sharedPref;
    String userLogin = "admin";
    String passLogin = "123";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AssetManager asset = getAssets();
        try {
            InputStream inImg = asset.open("Lighhouse.jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }

        // khởi tạo shared preferences để lưu user pass
        sharedPref = getSharedPreferences("sharePref",MODE_PRIVATE);
        // khởi tạo các control đặt tên các đối tượng trong màn hình
        createObject();
        // load data đã lưu user pass qua shared preferences
        loadData();
        // bắt sự kiện click button login
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // nếu chọn ghi nhớ
                if(cbRemember.isChecked()){
                    // thì lưu lại: viết hàm lưu lại, truyền user pass nhập vào và lưu lại
                    saveData(editUser.getText().toString(),editPass.getText().toString());
                }else {
                    // ko chọn ghi nhớ thì xóa thông tin, viết hàm xóa
                    clearData();
                }
                // nếu thông tin đúng thì mở màn hình Home báo đăng nhập thành công
                // thông tin user pass phải khai báo trước cho dễ
                if(editUser.getText().toString().equals(userLogin)&&
                        editPass.getText().toString().equals(passLogin)){
                    Intent intent = new Intent(MainActivity.this,HomeActivity.class);
                    startActivity(intent);
                }else {
                    // nếu nhập sai thì lên thông báo
                    Toast.makeText(MainActivity.this,"Thông tin không hợp lệ",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
    private void createObject(){
        editUser = (EditText) findViewById(R.id.editUser);
        editPass = (EditText) findViewById(R.id.editPass);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        cbRemember = (CheckBox) findViewById(R.id.cbRemember);
    }
    private void loadData(){
        // kiểm tra shared preference đúng key hay ko -
        // ko đúng key thì gán key vào biến, check remember
        // ngược lại thì ko check remember
        if(sharedPref.getBoolean("remember",false)){
            editUser.setText(sharedPref.getString("USERNAME",""));
            editPass.setText(sharedPref.getString("PASS", ""));
            cbRemember.setChecked(true);
        }
        else
        {
            cbRemember.setChecked(false);
        }

    }
    private void saveData(String username, String pass){
        // lưu lại
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("USERNAME", username);
        editor.putString("PASS",pass);
        editor.putBoolean("REMEMBER",cbRemember.isChecked());
        editor.commit();
    }
    private void clearData(){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.commit();
    }
}
