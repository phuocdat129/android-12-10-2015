package khtn.models;

public class HinhVuong extends DaGiac{
	public HinhVuong(int a)
	{
		super(a);
	}
	@Override
	public double TinhDienTich() {
		// TODO Auto-generated method stub
		return Math.pow(this.getA(), 2);
	}
	@Override
	public double TinhChuVi() {
		// TODO Auto-generated method stub
		return this.getA()*4;
	}
}
