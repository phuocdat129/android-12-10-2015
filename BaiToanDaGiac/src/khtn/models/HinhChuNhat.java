package khtn.models;

public class HinhChuNhat extends DaGiac{
	public HinhChuNhat(int a,int b)
	{
		super(a, b);
	}
	@Override
	public double TinhDienTich() {
		// TODO Auto-generated method stub
		return this.getA()*this.getB();
	}
	@Override
	public double TinhChuVi() {
		// TODO Auto-generated method stub
		return 2*(this.getA()+this.getB());
	}
}
