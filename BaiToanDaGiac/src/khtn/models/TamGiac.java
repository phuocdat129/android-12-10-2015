package khtn.models;

public class TamGiac extends DaGiac{
	public TamGiac(int a,int b,int c)
	{
		super(a, b, c);
	}
	@Override
	public double TinhChuVi() {
		return this.getA()+this.getB()+this.getC();
	}
	@Override
	public double TinhDienTich() {
		double p = TinhChuVi()/2;
		double s = Math.sqrt(p*(p-this.getA())*
				(p-this.getB())*(p-this.getC()));
		return s;
	}
}
