package khtn.main;

import java.util.ArrayList;

import khtn.models.DaGiac;
import khtn.models.HinhChuNhat;
import khtn.models.HinhVuong;
import khtn.models.TamGiac;

public class XuLyDaGiac {

	public static void main(String[] args) {
		// khai báo mảng dsDaGiac, sau đó thêm các phần tử mảng, tính tổng các phần tử
		ArrayList<DaGiac> dsDaGiac = new ArrayList<DaGiac>();
		
		// khai báo ds các đa giác sẽ thêm vào dsDaGiac để tính tổng		
		HinhChuNhat hcn1= new HinhChuNhat(4, 5);
		HinhChuNhat hcn2= new HinhChuNhat(3, 6);		
		HinhVuong hv1 = new HinhVuong(6);
		HinhVuong hv2 = new HinhVuong(4);		
		TamGiac tg1 = new TamGiac(3, 4, 5);
		// thêm các phần tử của mảng dsDaGiac có các đa giác sau:
		dsDaGiac.add(hcn1);
		dsDaGiac.add(hcn2);
		dsDaGiac.add(hv1);
		dsDaGiac.add(hv2);
		dsDaGiac.add(tg1);
		// tính tổng các phần tử trong dsDaGiac bằng vòng lặp for 
		// liệt kê từ phần tử đầu đến cuối của mảng dsDaGiac và cộng lại với nhau
		double tongDienTich =0.0;
		for (DaGiac daGiac : dsDaGiac) {
			tongDienTich+=daGiac.TinhDienTich();
		}
		System.out.println("Tổng diện tích là : "+tongDienTich);
	}

}
