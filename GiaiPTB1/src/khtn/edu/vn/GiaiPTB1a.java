package khtn.edu.vn;

import java.util.Scanner;

public class GiaiPTB1a 
{	
	static int a;
	static int b;
	
	public static void main(String[] args) 
		{
			// TODO Auto-generated method stub
			NhapHeSo();
			GiaiPT();
		}

		private static void NhapHeSo() 
		{
			// TODO Auto-generated method stub
			System.out.println("Ứng dụng giải phương trình bậc 1: ax + b = 0");
			Scanner sc = new Scanner(System.in);
			
			System.out.println("Mời bạn nhập hệ số a = ");		
			int a = sc.nextInt();
			
			System.out.println("Mời bạn nhập hệ số b = ");		
			int b = sc.nextInt();
			sc.close();
			System.out.println("Phương trình bạn đã nhập là : "+a+"x + "+b+" = 0");
		}

		private static void GiaiPT() 
		{
			// TODO Auto-generated method stub
			if(a==0)
			{
				if(b!=0)
				{
					System.out.println("Phương trình có VÔ NGHIỆM");
				}else
				{					
					System.out.println("Phương trình VÔ SỐ NGHIỆM");
				}
			}else
			{
				System.out.println("Phương trình CÓ NGHIỆM X = ");
				}
		}
}
