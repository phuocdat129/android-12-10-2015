package khtn.edu.vn;

import java.util.Scanner;

public class TuLamGiaiPTB1 {
	
	static int hsa;
	static int hsb;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Giải phương trình ax + b = 0 ");
		NhapHeSo();
		GiaiPT();
	}

	private static void NhapHeSo() {
		// TODO Auto-generated method stub
		Scanner nhap = new Scanner(System.in);
		System.out.println("Mời bạn nhập hệ số a: ");
		hsa = nhap.nextInt();
		System.out.println("Mời bạn nhập hệ số b: ");
		hsb = nhap.nextInt();
	}

	private static void GiaiPT() {
		// TODO Auto-generated method stub
		System.out.println("Phương trình của bạn là: "+hsa+"x + "+hsb+" = 0");
		
		if(hsa==0){
			if(hsb!=0){
				System.out.println("Phương trình VÔ NGHIỆM");
			}else{
				System.out.println("Phương trình VÔ SỐ NGHIỆM");
			}
			
			}
		else{
			float kq = (float)(-(hsb*1.0))/hsa;
			System.out.println("Phương trình có NGHIỆM X ="+kq);

		}
	}

}
