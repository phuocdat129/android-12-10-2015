package khtn.edu.vn;

import java.util.Scanner;

public class GiaiB1 {

	static int hsA;
	static int hsB;
	
	
	public static void main(String[] args) {
		NhapHeSo();
		GiaiPTBac1();
	}
	
	public static void NhapHeSo()
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhap hs A : ");
		hsA = sc.nextInt();
		System.out.println("Nhap hs B : ");
		hsB = sc.nextInt();
		System.out.println("A : "+hsA+" ; " +
				"B : "+hsB);
	}
	public static void GiaiPTBac1()
	{
		if(hsA==0)
		{
			if(hsB!=0)
			{
				System.out.println("Phuong Trinh Vo Nghiem");
			}
			else
			{
				System.out.println("PT Co Vo So Nghiem");
			}
		}
		else
		{
			float kq = (float) ((-hsB*1.0)/hsA);
			System.out.println("Nghiem cua PT la : "+kq);
		}
	}
	
}
