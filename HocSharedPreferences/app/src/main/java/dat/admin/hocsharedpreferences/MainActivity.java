package dat.admin.hocsharedpreferences;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

//    Khi chạy dứng ụng này sẽ hiện màng hình đăng nhập.
//        Người dùng nhập username và password vào, nếu đến sẽ chuyển đến màng hình khác,
//          thông báo đăng nhập thành công.
//        Ở màng hình đăng nhập có 1 checkbox, nếu nó được check lần sau, khi mở ứng dụng...
//          nó sẽ load lại username và password đã đăng nhập lần trước.
public class MainActivity extends AppCompatActivity
{
    EditText editUser, editPass;
    Button btnLogin;
    CheckBox cbRemember;
    SharedPreferences sharedPref;
    String userLogin = "admin";
    String passLogin = "123";

    public static final String MyPref = "MyPref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // khởi tạo shared preferences
        sharedPref = getSharedPreferences(MyPref, Context.MODE_PRIVATE);
        //khởi tạo các control: đặt tên các đối tượng như user, pass, checkbox, button login
        initWidgets();
        //lấy dữ liệu đã lưu nếu có
        loadData();
        //khi click button đăng nhập
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // nếu người dùng chọn có ghi nhớ
                if(cbRemember.isChecked())
                {
                    // ghi nhớ thì lưu lại thông tin đăng nhập
                    saveData(editUser.getText().toString(),editPass.getText().toString());
                }else
                {
                    clearData(); // ko ghi nhớ thì xóa thông tin đã lưu
                }
                // nếu thông tin đăng nhập đúng thì đến màn hình home, tạo màn hình home
                if(editUser.getText().toString().equals(userLogin)
                        &&editPass.getText().toString().equals(passLogin))
                {
                    Intent intent = new Intent(MainActivity.this,HomeActivity.class);
                    startActivity(intent);
                }else // thông tin đăng nhập sai thì thông báo
                {
                    Toast.makeText(MainActivity.this,"Thông tin không hợp lệ",
                                    Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    // hàm khởi tạo các control
    private void initWidgets() {
        editUser = (EditText) findViewById(R.id.edtUser);
        editPass = (EditText) findViewById(R.id.edtPass);
        cbRemember = (CheckBox) findViewById(R.id.chRemember);
        btnLogin = (Button) findViewById(R.id.btnLogin);
    }
    // hàm xóa dữ liệu có sẵn trên SharePref
    private void clearData() {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.commit();
    }
    // hàm lưu dữ liệu lên SharePref
    private void saveData(String username, String Pass) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("USERNAME", username);
        editor.putString("PASS", Pass);
        editor.putBoolean("REMEMBER",cbRemember.isChecked());
        editor.commit();
    }
    // hàm lấy dữ liệu đã lưu nếu có
    private void loadData() {
        if(sharedPref.getBoolean("remember",false)) {
            editUser.setText(sharedPref.getString("USERNAME", ""));
            editPass.setText(sharedPref.getString("PASS", ""));
            cbRemember.setChecked(true);
        }
        else
            cbRemember.setChecked(false);

    }
}
