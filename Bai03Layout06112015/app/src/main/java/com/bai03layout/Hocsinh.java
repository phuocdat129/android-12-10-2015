package com.bai03layout;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by PC14-02 on 10/30/2015.
 */
public class Hocsinh implements Parcelable{
    private String name;
    private String address;

    public Hocsinh() {
    }

    public static final Creator<Hocsinh> CREATOR = new Creator<Hocsinh>() {
        @Override
        public Hocsinh createFromParcel(Parcel in) {
            return new Hocsinh(in);
        }

        @Override
        public Hocsinh[] newArray(int size) {
            return new Hocsinh[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    protected Hocsinh(Parcel in) {
        address = in.readString();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(address);
        dest.writeString(name);

    }

    @Override
    public int describeContents() {
        return 0;
    }

}
