package com.bai03layout;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;

/**
 * Created by PC14-02 on 10/30/2015.
 */
public class Activity2 extends Activity implements View.OnClickListener{

    static final int REQUEST_CODE_CAMEAR = 10;

    View sms;
    String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity2);
        //
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        Bundle bundle = intent.getExtras();
        int year = bundle.getInt("year");
        Hocsinh hs = bundle.getParcelable("hs");

        System.out.println(name);
        System.out.println(year);
        System.out.println(hs.getName());

        View camera, viewimage, myimage;

        sms = findViewById(R.id.smstextView);
        camera = findViewById(R.id.button2);
        viewimage = findViewById(R.id.button3);
        myimage = findViewById(R.id.button4);

        sms.setOnClickListener(this);
        camera.setOnClickListener(this);
        viewimage.setOnClickListener(this);
        myimage.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Intent intent;

        switch (v.getId()){
            case R.id.smstextView:

                //action
                intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("smsto:1233645;3546465"));
                intent.putExtra("sms_body", "abckj asfjhs as v");
                startActivity(intent);

                break;

            case R.id.button2:

                //path image
                path = Environment.getExternalStorageDirectory() + "/tam.jpg";

                System.out.println(path);

                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.parse(path));

                if (intent.resolveActivity(getPackageManager()) != null) {
                    //startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);

                    //startActivity(intent);
                    startActivityForResult(intent, REQUEST_CODE_CAMEAR);
                }

                break;

            case R.id.button3:
                //Koala
                String s = "file://" + Environment.getExternalStorageDirectory() + "/Koala.jpg";
                System.out.println(s);
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(s), "image/*");

                if(intent.resolveActivity(getPackageManager())!=null) {
                    Intent chooser = Intent.createChooser(intent, "aaaaaa");
                    startActivity(chooser);
                }
                break;

            case R.id.button4:

                String s1 = Environment.getExternalStorageDirectory() + "/Koala.jpg";
                intent = new Intent(this, ImageActivity.class);
                intent.setData(Uri.parse(s1));

                startActivity(intent);

                break;

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE_CAMEAR){

            if (resultCode == Activity.RESULT_OK){

                System.out.println("co hinh");

            }
            else{
                System.out.println("K co hinh");
            }

        }

    }
}
