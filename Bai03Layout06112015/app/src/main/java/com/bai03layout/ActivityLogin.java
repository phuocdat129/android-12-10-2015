package com.bai03layout;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by PC14-02 on 11/6/2015.
 */
public class ActivityLogin extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_login);

        LinearLayout mainlayout = (LinearLayout) findViewById(R.id.mainLayout);
        TextView tvTitle = (TextView) findViewById(R.id.titleTextView);
        View loginBtn = findViewById(R.id.dangnhapButton);
        final EditText ten = (EditText) findViewById(R.id.tenEditText);
        final EditText matkhau = (EditText) findViewById(R.id.matkhauEditText);
        //Penguins.jpg

        AssetManager asset = getAssets();
        try {
            InputStream inImg= asset.open("Penguins.jpg");
            Bitmap bmp = BitmapFactory.decodeStream(inImg);
            BitmapDrawable drawable = new BitmapDrawable(bmp);

            mainlayout.setBackground(drawable);

            //
            Typeface typeface = Typeface.createFromAsset(asset,
                    "fonts/font1.ttf");

            tvTitle.setTypeface(typeface);

            /*AssetFileDescriptor dt = asset.openFd("abc.mp3");
            MediaPlayer player = new MediaPlayer();
            //set datasource.
            player.setDataSource(dt.getFileDescriptor());
            //
            player.prepare();
            //play
            player.start();*/

        } catch (IOException e) {
            e.printStackTrace();
        }

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String sTen = ten.getText().toString();
                String sMK = matkhau.getText().toString();

                if(sTen.trim().length() >=3 && sMK.trim().length() >=3 ){

                    String msg = "";
                    if(sTen.equalsIgnoreCase("admin") && sMK.equalsIgnoreCase("123")){
                        msg = "Dang nhap thanh cong";

                        //
                        SharedPreferences preferences = getSharedPreferences("share1", MODE_PRIVATE);
                        SharedPreferences.Editor edit = preferences.edit();
                        edit.putString("name", sTen);
                        edit.putString("pass", sMK);

                        //edit.remove("name");

                        edit.commit();

                    }
                    else{
                        msg = "Ten/MK khong dung";
                    }
                    Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getBaseContext(), "Ten/Mk khong hop le", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

}
