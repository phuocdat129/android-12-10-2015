package com.bai03layout;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;

/**
 * Created by PC14-02 on 11/2/2015.
 */
public class ImageActivity extends Activity{

    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image);
        img = (ImageView) findViewById(R.id.imageView);

        Uri data = getIntent().getData();
        String p = data.getPath();

        Bitmap bm = BitmapFactory.decodeFile(p);

        img.setImageBitmap(bm);

    }
}
