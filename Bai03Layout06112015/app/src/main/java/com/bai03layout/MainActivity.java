package com.bai03layout;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.LevelListDrawable;
import android.graphics.drawable.ScaleDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.InputDevice;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.util.Scanner;

import static com.bai03layout.R.id.imageView;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener {

    int abc = 0;
    ImageView img;
    int state = 0;
    ImageView img4;
    Button btnscale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //gan giao dien vao choa acitivy
        setContentView(R.layout.activity_main);

        SharedPreferences share = getSharedPreferences("share1", MODE_PRIVATE);
        String name = share.getString("name", "no");
        String pass = share.getString("pass", "no");

        if(name.equalsIgnoreCase("admin") && pass.equalsIgnoreCase("123")){
            System.out.println("da login");
        }
        else{
            Intent intent = new Intent();
            intent.setClassName(this, "com.bai03layout.ActivityLogin");

            startActivity(intent);

            finish();
        }

        Button btn = (Button) findViewById(R.id.button);
        btn.setOnClickListener(this);

        img = (ImageView) findViewById(R.id.imageView);
        SeekBar bar = (SeekBar) findViewById(R.id.seekBar);

        bar.setMax(70);
        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int value, boolean fromUser) {
                LevelListDrawable drawable = (LevelListDrawable) img.getBackground();
                drawable.setLevel(value);

                ClipDrawable clip = (ClipDrawable) img4.getDrawable();
                clip.setLevel(value * 100);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ImageView img2 = (ImageView) findViewById(R.id.imageView2);
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView imgV = (ImageView) v;
                TransitionDrawable drawable = (TransitionDrawable) imgV.getBackground();
                if(state == 0){
                    state = 1;
                    drawable.startTransition(500);
                }
                else{
                    state = 0;
                    drawable.reverseTransition(500);
                }
            }
        });

        img4 = (ImageView) findViewById(R.id.imageView4);

        btnscale = (Button) findViewById(R.id.btnScale);
        SeekBar sbScale = (SeekBar) findViewById(R.id.seekBarScale);
        sbScale.setMax(100);

        sbScale.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ScaleDrawable drawable = (ScaleDrawable) btnscale.getBackground();
                drawable.setLevel(progress*100);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


/*
        //Context
        Context context = this.getBaseContext();
        int color = context.getResources().getColor(R.color.xanh);

        Resources res = this.getResources();

        String msg = res.getString(R.string.dialog_dangnhap_sai);

        int tinnhan = 5;
        String  msg_dau = res.getString(R.string.thongbao_dau);
        String  msg_duoi = res.getString(R.string.thongbao_duoi);

        System.out.println(msg_dau + tinnhan + msg_duoi);

        String s1 = res.getString(R.string.thongbao_tinnhan, 1, 15);
        System.out.println(s1);

        String s2 = res.getString(R.string.thongbao_tinnhan);
        String s3 = String.format(s2, 2, 8);
        System.out.println(s3);

        String s4 = res.getString(R.string.button_title);
        System.out.println(s4);

        //Html.format
        //btn.setText(Html.fromHtml(s4));

        String s5 = res.getQuantityString(R.plurals.sach, 1);
        String s6 = res.getQuantityString(R.plurals.sach, 2);

        System.out.println(s5);
        System.out.println(s6);

        String[] s7 = res.getStringArray(R.array.gioitinh);

        TelephonyManager tel = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);
        String mnc = tel.getNetworkOperator();

        System.out.println("mnc: " + mnc);

        LayerDrawable draw1 = (LayerDrawable) this.getResources().getDrawable(R.drawable.layer_drawable);*/

        /**
         * private folder
         * */
        File files = this.getFilesDir();
        File cache = getCacheDir();
        File abc = getDir("abc", MODE_APPEND);


        File f1 = new File(files, "a.txt");
        File f2 = new File(files.getAbsolutePath() + "/b.txt");

        File c1 = new File(cache, "c.txt");
        File c2 = new File(cache.getAbsolutePath() + "/d.txt");

        try {
            f1.createNewFile();
            f2.createNewFile();

            c1.createNewFile();
            c2.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(Environment.getExternalStorageState()!= Environment.MEDIA_MOUNTED ){

        }

        File externalStorage = Environment.getExternalStorageDirectory();
        System.out.println(externalStorage.getAbsolutePath());

        File folder1 = new File(externalStorage, "folder2");
        System.out.println(folder1.getAbsolutePath());
        boolean result = folder1.mkdir();

        File f3 = new File(folder1, "file1.txt");
        try {
            f3.createNewFile();

            LinearLayout layoutFiles = (LinearLayout) findViewById(R.id.layout_list_file);

            File[] arrFile = externalStorage.listFiles();
            for (File a : arrFile) {
                if(a.isDirectory()){
                    System.out.println("Fol: " + a.getAbsolutePath());
                }
                else{
                    System.out.println("File: " + a.getAbsolutePath());
                }

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                Button button = new Button(this);
                button.setText(a.getName());
                button.setLayoutParams(lp);

                layoutFiles.addView(button);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(result);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.button:

                Hocsinh hs = new Hocsinh();
                hs.setName("nguyen van teo");
                hs.setAddress("hcm");

                Bundle bundle = new Bundle();
                bundle.putInt("year", 2015);
                bundle.putParcelable("hs", hs);

                //this, Activity2.class
                Intent intent = new Intent();
                intent.setClass(this, Activity2.class);
                intent.putExtra("name", "nguyen van a");
                intent.putExtras(bundle);

                startActivity(intent);

                /*Uri uri = Uri.parse("");

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(uri);
                intent.setType("image*//*");

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);*/

                break;
        }

    }
}