package dat.app3010;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity
    implements View.OnClickListener
{
    Intent intentSL, intentLL;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnToSL = (Button) findViewById(R.id.button1sl);
        Button btnToLL = (Button) findViewById(R.id.button2ll);

        btnToLL.setOnClickListener(this);
        btnToSL.setOnClickListener(this);

        intentSL = new Intent(this, StateList.class);
        intentLL = new Intent(this, LevelList.class);

        /*
        btnToSL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intentSL);
                finish();
            }
        });
        */

        /*
        btnToLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intentLL);
                finish();
            }
        });
        */
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.button1sl:
                startActivity(intentSL);
                break;
            case R.id.button2ll:
                startActivity(intentLL);
                break;
        }

    }
    //SeekBar.OnSeekBarChangeListener
}
