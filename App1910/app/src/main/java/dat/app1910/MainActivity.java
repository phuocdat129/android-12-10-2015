package dat.app1910;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Intent intentDangKy, intentDangNhap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnToDangNhap = (Button) findViewById(R.id.buttondn);
        Button btnToDangKy = (Button) findViewById(R.id.buttondk);

        intentDangKy = new Intent(this, Main3Activity.class);
        intentDangNhap = new Intent(this, Main2Activity.class);

        btnToDangKy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intentDangKy);
                finish();
            }
        });

        btnToDangNhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intentDangNhap);
                finish();
            }
        });

    }
}
