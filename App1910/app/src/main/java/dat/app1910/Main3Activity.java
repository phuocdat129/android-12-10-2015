package dat.app1910;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main3Activity extends AppCompatActivity {
    Intent intentTrangChu;
    TextView editTen, editPass, editRePass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        Button btnToTrangChu = (Button) findViewById(R.id.buttontc);
        intentTrangChu = new Intent(this, MainActivity.class);
        btnToTrangChu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intentTrangChu);
            }
        });

        editTen = (TextView) findViewById(R.id.editTen);
        editPass = (TextView) findViewById(R.id.editPass);
        editRePass = (TextView) findViewById(R.id.editrePass);
        Button btnToDangKy = (Button) findViewById(R.id.buttondk);

        btnToDangKy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ten = editTen.getText().toString();
                String pass = editPass.getText().toString();
                String repass = editRePass.getText().toString();

                if(ten.isEmpty())
                {
                    ThongBao("Chưa nhập tên",false);
                }else if (pass.isEmpty() || repass.isEmpty())
                {
                    ThongBao("Chưa nhập pass",false);
                }else if (pass.contentEquals(repass)==false)
                {
                    ThongBao("Nhập pass không giống nhau, vui lòng nhập lại pass",false);
                }else
                {
                    GhiFile(ten + "-" +pass);
                    ThongBao("Đăng ký thành công: "+"{"+ten+","+pass+"}",true);
                }

            }
            public void ThongBao(String noiDung, Boolean exitOrNot){
                AlertDialog.Builder msg;
                msg = new AlertDialog.Builder(Main3Activity.this);
                msg.setTitle("Thông báo!");
                if (exitOrNot.booleanValue()) {
                    msg.setMessage(noiDung);
                    msg.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                }else {
                    msg.setMessage(noiDung);
                    msg.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                }
                msg.show();
            }

            public void GhiFile(String s){
                try {
                    FileOutputStream output = openFileOutput("UserFile", MODE_PRIVATE);
                    char[] chars = s.toCharArray();
                    for (int i = 0; i < chars.length; i++) {
                        output.write(chars[i]);
                    }
                    output.close();
                } catch (FileNotFoundException e) {
                    ThongBao("Không tìm thấy file", false);
                } catch (IOException e) {
                    ThongBao("Không thể ghi file", false);
                }
            }


        });




    }
}
