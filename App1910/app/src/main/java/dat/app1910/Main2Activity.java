package dat.app1910;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Main2Activity extends AppCompatActivity {
    Intent intentTrangChu;
    Intent intentDangNhap;
    TextView editTen, editPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Button btnToTrangChu = (Button) findViewById(R.id.buttontc);
        Button btnToDangNhap = (Button) findViewById(R.id.buttondn);
        editTen = (TextView) findViewById(R.id.editTen);
        editPass = (TextView) findViewById(R.id.editPass);

        intentTrangChu = new Intent(this, MainActivity.class);
        btnToTrangChu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intentTrangChu);
            }
        });

        btnToDangNhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ten = editTen.getText().toString();
                String pass = editPass.getText().toString();
                if (ten.isEmpty() || pass.isEmpty()) {
                    thongBao("Chưa nhập tên", false);
                } else {
                    docFile();
                    thongBao("Đăng nhập thành công: " + "{" + ten + "," + pass + "}", true);
                }
            }
        });
    }

    public void thongBao(String noiDung, Boolean exitOrNot){
        AlertDialog.Builder msg = new AlertDialog.Builder(this);
        msg.setTitle("Thông báo!");
        if (exitOrNot.booleanValue()) {
            msg.setMessage(noiDung);
            msg.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        }else {
            msg.setMessage(noiDung);
            msg.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        msg.show();
    }

    public String[] docFile(){
        try {
            FileInputStream input = openFileInput("UserFile");
            String doc = "";
            int count = input.available();
            for (int i = 0; i < count; i++) {
                doc += (char) input.read();
            }
            String[] user = doc.split("-");
            input.close();
            return user;
        } catch (FileNotFoundException e) {
            thongBao("Không tìm thấy file",false);
            return null;
        } catch (IOException e) {
            thongBao("Không thể đọc file",false);
            return null;
        }
    }


}


